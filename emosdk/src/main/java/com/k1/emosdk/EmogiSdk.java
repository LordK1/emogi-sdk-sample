package com.k1.emosdk;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.k1.emosdk.model.Data;
import com.k1.emosdk.model.Device;
import com.k1.emosdk.model.EmogiAdFormat;
import com.k1.emosdk.model.Geo;
import com.k1.emosdk.model.User;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * <h2>The Emogi SDK to make it produce an api for integrator applications
 * to use features of the sdk.</h2>
 * <p>
 * <p>Implementation note:
 * for instantiating
 * <code>
 * new EmogiSdk.Builder(this)
 * .promptPermissions(true)
 * .setLocation(LocationServices.FusedLocationApi.getLastLocation(apiClient))
 * .logger(true)
 * .setGoogleApiClient(apiClient)
 * .setDeviceId("Sample")
 * .setmUser(new User(22, User.Gender.MALE))
 * .build();
 * }
 * </code>
 * </p>
 * <p>
 * In other activities or use cases you can use {@link #getDefaultTracker()}
 * or your declared instance on the MainApplication
 * i.e :
 * <code>
 * public EmogiSdk getDefaultEmogi() {
 * return mEmogiSdk;
 * }
 * </code>
 * <p>
 * Created by K1 on 5/13/17.
 *
 * @see Location
 * @see Builder
 * @see Data
 */
public final class EmogiSdk {

    private static final String TAG = EmogiSdk.class.getSimpleName();
    private static EmogiSdk instance;

    private final Context context;
    private final String appId;
    private final User user;
    private final ApplicationInfo mApplicationInfo;
    private final float batteryPercent;
    private final Gson gson = new Gson();
    private Location lastLocation;
    private String devId;

    /**
     * @param context
     * @param lastLocation
     * @param appId
     * @param user
     * @param mApplicationInfo
     * @param batteryPercent
     */
    EmogiSdk(Context context, Location lastLocation, String appId, User user, ApplicationInfo mApplicationInfo, float batteryPercent) {
        this.context = context;
        this.lastLocation = lastLocation;
        this.appId = appId;
        this.user = user;
        this.mApplicationInfo = mApplicationInfo;
        this.batteryPercent = batteryPercent;
    }

    /**
     * @param builder
     */
    EmogiSdk(Builder builder) {
        this(builder.context,
                builder.mLastLocation,
                builder.devID,
                builder.mUser,
                builder.mApplicationInfo,
                builder.mBatteryPercent);
    }

    /**
     * @return
     */
    private static EmogiSdk getInstance() {
        synchronized (EmogiSdk.class) {
            if (instance == null) {
                throw new IllegalArgumentException(instance + " is null !!!");
            }
        }
        return instance;
    }

    /**
     * To get the default instance of the {@link EmogiSdk}
     *
     * @return
     */
    public static EmogiSdk getDefaultTracker() {
        return getInstance();
    }

    /**
     * Just be aware of we need single instance of {@link EmogiSdk} at the moment !
     * NOTICE: this method must not be called some where else.
     *
     * @param builder
     * @return
     */
    private static EmogiSdk init(Builder builder) {
        synchronized (EmogiSdk.class) {
            if (instance == null) {
                instance = new EmogiSdk(builder);
            }
        }

        return instance;
    }

    /**
     * Send collected data and pass it to the server
     */
    public void getData(EmogiResponseHandler handler) {
        Log.d(TAG, "getData() called"
                + " instance : " + instance);
        if (instance == null) throw new NullPointerException("INSTANCE MUST NOT BE NULL !!!");
        // Collected data
        final Data data = instance.collectData();
        final String jsonData = getJSONData(data);
        Log.d(TAG, "getData: >>>> data : " + data
                + " jsonData : " + jsonData
                + " logBuildInfo: " + logInfo()
        );
        // Configurable the client
        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new LoggerInterceptor())
                .build();

        // Declare and execute request
        final RequestBody requestBody = RequestBody.create(
                MediaType.parse("application/json; charset=utf-8"),
                jsonData
        );
        // make the request
        final Request request = new Request.Builder().url(BuildConfig.BASE_URL)
                .post(requestBody)
                .build();
        // enqueue asynchronous process
        okHttpClient.newCall(request).enqueue(new PostDataCallback(handler));
    }

    /**
     * All the deivce infos
     *
     * @return
     */
    private String logInfo() {
        return new StringBuilder()
                .append("Build.BOARD: " + Build.BOARD)
                .append("Build.TYPE: " + Build.TYPE)
                .append("Build.DISPLAY: " + Build.DISPLAY)
                .append("Build.RADIO: " + Build.RADIO)
                .append("Build.BRAND: " + Build.BRAND)
                .append("Build.BOOTLOADER: " + Build.BOOTLOADER)
                .append("Build.DEVICE: " + Build.DEVICE)
                .append("Build.HARDWARE: " + Build.HARDWARE)
                .append("Build.getRadioVersion: " + Build.getRadioVersion())
                .append("Build.MANUFACTURER: " + Build.MANUFACTURER)
                .append("Build.MODEL: " + Build.MODEL)
                .append("Build.VERSION.BASE_OS: " + Build.VERSION.BASE_OS)
                .append("Build.VERSION.CODENAME: " + Build.VERSION.CODENAME)
                .append("Build.VERSION.RELEASE: " + Build.VERSION.RELEASE)
                .append("Build.VERSION.SDK_INT: " + Build.VERSION.SDK_INT)
                .append("Build.VERSION_CODES: " + Build.PRODUCT)
                .append("Build.VERSION_CODES: " + Build.SERIAL)
                .append("Build.VERSION_CODES: " + Build.ID)
                .toString();

    }

    /**
     * To expose all collected data and a mixture with integrator data
     *
     * @return
     */
    private Data collectData() {
        final Data data = new Data();
        data.setAppId(instance.appId);
        data.setDevIdType("IDFA");
        final String[] formates = {
                "gif",
                "emo",
                "a-emo",
                "sti",
                "a-sti"
        };

        data.setFormates(Arrays.asList(formates));
        data.setGeo(new Geo(lastLocation));
        data.setDevice(new Device(context));
        data.setUser(user);
        return data;
    }

    /**
     * to generate the JSON content for post request body
     *
     * @return
     */
    private String getJSONData(Data data) {
        return gson.toJson(data);
    }

    /**
     * To update the current location or last updated location
     *
     * @param location
     */
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged() called with: location = [" + location + "]"
                + " instance : " + instance
                + " instance location : " + instance.lastLocation
        );
        if (instance.lastLocation != null) { // just for preventing the duplication and replication
            instance.lastLocation.set(location);
        } else {
            instance.lastLocation = location;
        }
    }

    /**
     * @param devId
     */
    public void setDevId(String devId) {
        this.devId = devId;
    }


    /**
     * to implement Builder of the {@link EmogiSdk}, and handle the required fields/properties
     * for {@link EmogiSdk} instantiating and initializing the instance.
     *
     * @author K1
     */
    public static class Builder {
        public static final int REQUEST_CODE = 1009;
        private final Context context;
        // To request for required permissions automatically by the sdk side
        private boolean mAutoPermission;
        // track the last location
        private Location mLastLocation;
        // App ID
        private String devID;
        private boolean isLogEnabled;
        // User data
        private User mUser;
        // Meta Data
        private ApplicationInfo mApplicationInfo;
        // Battery Percent
        private float mBatteryPercent;
        private EmogiActivityLifecycleCallbacks mActivityLifecycle = new EmogiActivityLifecycleCallbacks();
        private BatterStatusBroadcastReceiver mBatteryReceiver;
        private String appId;
        private ArrayList<String> formats = new ArrayList<>();

        /**
         * @param context
         */
        public Builder(Context context) {
            if (context == null)
                throw new IllegalArgumentException("Context must not be null.");
            else
                this.context = context;


        }

        /**
         * @param autoPermission true value means that integrator wanted to sdk give required permission on the start
         * @return
         */

        public Builder promptPermissions(boolean autoPermission) {
            this.mAutoPermission = autoPermission;
            return this;
        }


        /**
         * Set the latest Location from the app
         *
         * @param lastLocation
         * @return
         */
        public Builder setLocation(Location lastLocation) {
            this.mLastLocation = lastLocation;
            return this;
        }


        /**
         * set the devID
         *
         * @param deviceId
         * @return
         */
        public Builder setDeviceId(String deviceId) {
            this.devID = deviceId;
            return this;
        }


        /**
         * No we have to some double check for required fields and fill some of them !!!
         *
         * @return
         */
        public EmogiSdk build() {
            try {
                // Application Info and MetaData
                mApplicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
                Log.d(TAG, "build: >>>> META DATA: " + mApplicationInfo.metaData + " =-=-=-=");

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            /*It should have been unnecessary, but we need it to track some activity life cycles */
            ((Application) context).registerActivityLifecycleCallbacks(mActivityLifecycle);


            if (mLastLocation == null) {
                Log.d(TAG, "build: =-=- last location did not defined on the integrator -=-=-");
                final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                final List<String> allProviders = locationManager.getAllProviders();
                Log.d(TAG, "build: >>> allProviders : " + allProviders);
                final Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                criteria.setAltitudeRequired(false);
                criteria.setBearingRequired(false);
                criteria.setCostAllowed(true);
                criteria.setPowerRequirement(Criteria.POWER_LOW);

                final String bestProvider = locationManager.getBestProvider(criteria, false);
                Log.d(TAG, "build: >>> bestProvider : " + bestProvider);
                try {
                    if (bestProvider != null) {
                        final Location lastKnownLocation = locationManager.getLastKnownLocation(bestProvider);
                        Log.d(TAG, "build: =-=-= lastKnownLocation : " + lastKnownLocation + " =--=-=");
                        if (lastKnownLocation != null) {
                            Log.d(TAG, "build: =-=-=It was unexacting OH MA GOSH LAST KNOWN LOCATION FOUND HERE =-=-=");
                            this.mLastLocation = lastKnownLocation;
                        } else { // now I'm gonna get the location from the network provider
                            final LocationListener locationListener = new LocationListener(locationManager);
                            /* We’re using just Network provider and get the last  */
                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, locationListener);
                        }

                    } else { // It's because mUser did not accept the permissions !!!
                        Log.e(TAG, "build: bestProvider is NULL !!!");
                    }
                } catch (SecurityException e) { // permissions is not checked !?!?
                    e.printStackTrace();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermission(mActivityLifecycle.mCurrentActivity,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION);
                    }

                }

            }
            // if integrator did not provide the value of appId, I've just decided to claim package name as the appId
            if (TextUtils.isEmpty(this.appId)) {
                this.appId = context.getPackageName();
            }
            //  if integrator did not provide the value of appId, I've just decided to claim AAID ( as the devId )
            if (TextUtils.isEmpty(this.devID)) {
                Log.e(TAG, "build: =-=-= devID not Found =-=-=-=");
            }
            //  if integrator did not provide the value of formats, I've just decided to ALL
            if (formats.isEmpty()) {
                setAdFormats(EmogiAdFormat.ALL);
            }
            // ConnectivityStatus
            checkConnectivityStatus(context);
            // Battery Status
            checkBatteryStatus(context);
            // now we just initialize the single instance of EmogiSdk with Builder information
            return EmogiSdk.init(this);
        }

        /**
         * To send a permission request for the current activity
         *
         * @param activity
         * @param args
         */
        @RequiresApi(api = Build.VERSION_CODES.M)
        private void requestPermission(Activity activity, String... args) {
            if (activity != null) {
                checkPermission(activity, args);
                activity.requestPermissions(args, REQUEST_CODE);
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.M)
        private void checkPermission(Activity activitie, String[] args) {
            for (String permission : args) {
                final boolean rationale = activitie.shouldShowRequestPermissionRationale(permission);
                Log.d(TAG, "checkPermission: =-=-= rationale : " + rationale + " permission : " + permission);
            }
        }

        /**
         * To check and figure it our the percentage of batter of the device
         *
         * @param context
         */
        private void checkBatteryStatus(Context context) {
            Log.d(TAG, "checkBatteryStatus() called with: context = [" + context + "]");
            try {
                mBatteryReceiver = new BatterStatusBroadcastReceiver();
                context.registerReceiver(mBatteryReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
            } catch (SecurityException e) {
                e.printStackTrace();
            }

        }

        /**
         * @param context
         */
        private void checkConnectivityStatus(Context context) {
            try {
                final ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                final NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                Log.d(TAG, "checkConnectivityStatus: >>>  " + activeNetworkInfo
                        + " getTypeName: " + activeNetworkInfo.getTypeName()
                        + " getExtraInfo: " + activeNetworkInfo.getExtraInfo()
                        + " getDetailedState: " + activeNetworkInfo.getDetailedState()
                        + " getReason: " + activeNetworkInfo.getReason()
                        + " isAvailable: " + activeNetworkInfo.isAvailable()
                        + " getState: " + activeNetworkInfo.getState()
                );
            } catch (SecurityException e) { // again we need the permissions
                e.printStackTrace();
            }
        }


        /**
         * @param isLogEnabled
         * @return
         */
        public Builder logger(boolean isLogEnabled) {
            this.isLogEnabled = isLogEnabled;
            return this;
        }

        public Builder setmUser(User mUser) {
            this.mUser = mUser;
            return this;
        }

        /**
         * Set the battery percentage value and unregister the receiver
         *
         * @param percent
         */
        private void setBatteryPercent(float percent) {
            this.mBatteryPercent = percent;
            if (mBatteryReceiver != null) { // check this out
                context.unregisterReceiver(mBatteryReceiver);
            }
        }

        /**
         * To define the appId field
         *
         * @param appId {@link String} value of the app Id
         * @return {@link Builder}
         */
        public Builder setAppID(String appId) {
            this.appId = appId;
            return this;
        }


        /**
         * Formats
         * <p>
         * "gif",
         * "emo",
         * "a-emo",
         * "sti",
         * "a-sti"
         * </p>
         *
         * @param format
         * @return
         */
        public Builder setAdFormats(EmogiAdFormat format) {
            final ArrayList<String> strings = new ArrayList<>();
            switch (format) {
                case ALL:
                    strings.add("gif");
                    strings.add("emo");
                    strings.add("a-emo");
                    strings.add("sti");
                    strings.add("a-sti");
                    break;
                case GIF:
                    break;
                case EMOJI:
                    break;
                case STICKER:
                    break;
            }
            this.formats = strings;
            return this;
        }

        /**
         * It helps us to track the integrator application
         */
        private static class EmogiActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {
            private Activity mCurrentActivity;
            private boolean accessFineLocationGranted;
            private boolean coarseLocationGranted;


            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                Log.d(TAG, "onActivityCreated() called with: activity = [" + activity + "], savedInstanceState = [" + savedInstanceState + "]");
                this.mCurrentActivity = activity;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    accessFineLocationGranted = activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
                    coarseLocationGranted = activity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
                }

            }

            @Override
            public void onActivityStarted(Activity activity) {
                mCurrentActivity = activity;
                Log.d(TAG, "onActivityStarted() called with: activity = [" + activity + "]");
            }

            @Override
            public void onActivityResumed(Activity activity) {
//                Log.d(TAG, "onActivityResumed() called with: activity = [" + activity + "]");
                this.mCurrentActivity = activity;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    // check for permissions granted
                    if (!accessFineLocationGranted || !coarseLocationGranted) {
                        Log.d(TAG, "onActivityResumed: -=-=-= activity : "
                                + " has not either > ACCESS FINE LOCATION: " + accessFineLocationGranted
                                + " or Coarse Permission: " + coarseLocationGranted
                        );
                        // request the required permissions
                        requestPermission(activity,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION);

                    } else { // all the required permission granted
                        Log.i(TAG, "onActivityResumed: ========== " + accessFineLocationGranted + " ==========="
                                + " <<<< " + coarseLocationGranted + " >>>>>");
                    }
                }
                activity.getApplication().unregisterActivityLifecycleCallbacks(this);

            }


            /**
             * when you wanted to request the required permissions
             * by your side and integrator did not handle these permission or permissions !!!
             *
             * @param activity
             */
            private void requestPermission(Activity activity, String... args) {
                Log.d(TAG, "requestPermission() called with: activity = [" + activity + "], args = [" + args + "]");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activity.requestPermissions(args, REQUEST_CODE);
                }

            }

            @Override
            public void onActivityPaused(Activity activity) {
                Log.d(TAG, "onActivityPaused() called with: activity = [" + activity + "]");
            }

            @Override
            public void onActivityStopped(Activity activity) {
                Log.d(TAG, "onActivityStopped() called with: activity = [" + activity + "]");
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
                Log.d(TAG, "onActivitySaveInstanceState() called with: activity = [" + activity + "], outState = [" + outState + "]");
                activity.getApplication().unregisterActivityLifecycleCallbacks(this);
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                Log.d(TAG, "onActivityDestroyed() called with: activity = [" + activity + "]");
                activity.getApplication().unregisterActivityLifecycleCallbacks(this);
            }


        }

        /**
         * To check the latest location changes and update the {@link #mLastLocation}
         * We need just one location Or we have to all the changes, we know that
         * for track all location changes it is recommended that would be better to use broadcast
         * receivers.
         *
         * @author K1
         */
        private class LocationListener implements android.location.LocationListener {
            private final LocationManager locationManager;

            /**
             * @param locationManager
             */
            public LocationListener(LocationManager locationManager) {
                this.locationManager = locationManager;
            }

            @Override
            public void onLocationChanged(Location location) {
                Log.d(TAG, "onLocationChanged() called with: location = [" + location + "]");
                mLastLocation = location;
                // EmogiSdk.getInstance().onLocationChanged(mLastLocation);
                // After finding first location just remove the updates
                locationManager.removeUpdates(this);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d(TAG, "onStatusChanged() called with: provider = [" + provider + "], status = [" + status + "], extras = [" + extras + "]");
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.d(TAG, "onProviderEnabled() called with: provider = [" + provider + "]");
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.d(TAG, "onProviderDisabled() called with: provider = [" + provider + "]");
            }
        }

        /**
         * To check the battery status we're going to register the receiver
         */
        private class BatterStatusBroadcastReceiver extends BroadcastReceiver {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "onReceive() called with: context = [" + context + "], intent = [" + intent + "]");
                final int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
                final int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                // Calculate the battery charged percentage
                final float percent = (level / scale) * 100;
                Log.d(TAG, "onReceive: =-=-=- scale : " + scale + " =-= " + level + " =-=- percent : " + percent);
                setBatteryPercent(percent);
            }
        }
    }

    /**
     * This is simple Logger Interceptor to log some info about request and responses
     */
    private static class LoggerInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            final long t1 = System.nanoTime(); // start time
            final Request original = chain.request();
            final Request.Builder builder = original.newBuilder();
            // Headers
            builder.headers(new Headers.Builder()
//                    .add("Auth", "SomethingForAuthHeader") // to add some custom headers
                    .build());
            final Request request = builder.build();
            // Request info
            Log.d(TAG, String.format(Locale.getDefault(),
                    "Sending Request for : %s %n via Headers : %s %n"
                    , request.url()
                    , request.headers())
            );
            final Response response = chain.proceed(request); // sending request
            final long t2 = System.nanoTime();
            // Response logs
            Log.d(TAG, String.format(Locale.getDefault(),
                    "Response received from: %s %n in: %.1fms %n via status: %d , message: %s %n headers: %s %n ",
                    response.request().url(),
                    (t2 - t1) / 1e6d,
                    response.code(),
                    request.tag().toString(),
                    response.headers()
            ));
            return response;
        }
    }

    /**
     * When data getting responded from api call
     */
    private class PostDataCallback implements Callback {
        private final EmogiResponseHandler handler;

        /**
         * @param handler
         */
        public PostDataCallback(EmogiResponseHandler handler) {
            this.handler = handler;
        }

        @Override
        public void onFailure(Call call, IOException e) {
            Log.d(TAG, "onFailure() called with: call = [" + call + "], e = [" + e + "]");
            handler.onError(new EmogiError(e.getMessage()));
        }

        @Override
        public void onResponse(Call call, Response response) throws IOException {
            Log.d(TAG, "onResponse() called with: call = [" + call + "], response = [" + response + "]");
            final String string = response.body().string();
            final EmogiResponse emogiResponse = gson.fromJson(string, EmogiResponse.class);
            if (response.isSuccessful()) {
                Log.d(TAG, "onResponse: >>>> emogiResponse : " + emogiResponse + " <<<<");
                handler.onSuccess(emogiResponse);
            } else { // I've just received 500 Internal error, and tried to consider it as a message
                handler.onError(new EmogiError(emogiResponse));
            }
        }
    }
}
