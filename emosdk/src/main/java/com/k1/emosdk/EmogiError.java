package com.k1.emosdk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by K1 on 5/15/17.
 */
public class EmogiError {

    @SerializedName("error")
    @Expose
    private String message;

    public EmogiError(EmogiResponse emogiResponse) {
        this.message = emogiResponse.toString();
    }

    public EmogiError(String message) {
        this.message = message;
    }


    @Override
    public String toString() {
        return "EmogiError{" +
                "message='" + message + '\'' +
                '}';
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
