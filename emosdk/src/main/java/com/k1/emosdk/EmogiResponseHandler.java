package com.k1.emosdk;

/**
 * to return back response data into integrator app and handle some uses cases
 *
 * @author K1
 */
public interface EmogiResponseHandler {
    /**
     * when response getting successful response
     *
     * @param response
     */
    void onSuccess(EmogiResponse response);

    /**
     * When response getting unsuccessful
     *
     * @param error instance {@link EmogiError}
     */
    void onError(EmogiError error);

}
