package com.k1.emosdk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by K1 on 5/15/17.
 */
public class EmogiResponse {

    @SerializedName("request_id")
    @Expose
    private String requestId;

    @SerializedName("status")
    private String status;

    @SerializedName("data")
    @Expose
    private Object data;


    @Override
    public String toString() {
        return "EmogiResponse{" +
                "requestId='" + requestId + '\'' +
                ", status='" + status + '\'' +
                ", data=" + data +
                '}';
    }


    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
