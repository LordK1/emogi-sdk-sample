package com.k1.emosdk.model;

import android.location.Location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by K1 on 5/13/17.
 */
public class Geo {

    @SerializedName("lat")
    @Expose
    private double latittude;

    @SerializedName("lng")
    @Expose
    private double longtitude;

    public Geo(double latittude, double longtitude) {
        this.latittude = latittude;
        this.longtitude = longtitude;
    }

    public Geo(Location lastLocation) {
        this.latittude = lastLocation.getAltitude();
        this.longtitude = lastLocation.getLongitude();
    }

    public double getLatittude() {
        return latittude;
    }

    public void setLatittude(double latittude) {
        this.latittude = latittude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }
}
