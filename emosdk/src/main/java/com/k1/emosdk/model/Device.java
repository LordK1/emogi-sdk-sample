package com.k1.emosdk.model;

import android.content.Context;
import android.net.TrafficStats;
import android.os.Build;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by K1 on 5/13/17.
 */
public class Device {

    @SerializedName("dev_type")
    @Expose
    private String type;

    @SerializedName("batt")
    @Expose
    private int battery;

    @SerializedName("conn")
    @Expose
    private String connection;

    @SerializedName("os")
    @Expose
    private String os;

    @SerializedName("os_ver")
    @Expose
    private String version;

    @SerializedName("scr_w")
    @Expose
    private int width;

    @SerializedName("scr_h")
    @Expose
    private int height;

    @SerializedName("pxr")
    @Expose
    private int ratio;

    @SerializedName("ppi")
    @Expose
    private int ppi;

    /**
     * @param context
     */
    public Device(Context context) {
        this.type = Build.TYPE;
        this.os = Build.VERSION.BASE_OS;
        this.connection = String.valueOf(TrafficStats.getTotalTxBytes());
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public String getConnection() {
        return connection;
    }

    public void setConnection(String connection) {
        this.connection = connection;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getRatio() {
        return ratio;
    }

    public void setRatio(int ratio) {
        this.ratio = ratio;
    }

    public int getPpi() {
        return ppi;
    }

    public void setPpi(int ppi) {
        this.ppi = ppi;
    }
}
