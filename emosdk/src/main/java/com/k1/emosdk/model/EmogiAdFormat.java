package com.k1.emosdk.model;

/**
 * Created by K1 on 5/13/17.
 */
public enum EmogiAdFormat {
    ALL, GIF, EMOJI, STICKER
}
