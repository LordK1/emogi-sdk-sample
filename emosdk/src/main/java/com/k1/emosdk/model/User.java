package com.k1.emosdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by K1 on 5/13/17.
 */
public class User {

    @SerializedName("age")
    @Expose
    private int age;

    @SerializedName("gender")
    @Expose
    private String gender;

    public User(int age, Gender gender) {
        this.age = age;
        this.gender = gender.name();
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public enum Gender {
        MALE, FEMALE, UNKNOWN
    }
}
