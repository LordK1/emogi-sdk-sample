package com.k1.emosdk.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by K1 on 5/13/17.
 */
public class Data {

    @SerializedName("app_id")
    @Expose
    private String appId;

    @SerializedName("dev_id")
    @Expose
    private String devId;

    @SerializedName("dev_id_type")
    @Expose
    private String devIdType;

    @SerializedName("ad_fmts")
    @Expose
    private List<String> formates = new ArrayList<>();

    @SerializedName("geo")
    @Expose
    private Geo geo;

    @SerializedName("user")
    @Expose
    private User user;

    @SerializedName("dev")
    @Expose
    private Device device;

    public Data(String appId, String devId, String devIdType, List<String> formates,
                Geo geo, User user, Device device) {
        this.appId = appId;
        this.devId = devId;
        this.devIdType = devIdType;
        this.formates = formates;
        this.geo = geo;
        this.user = user;
        this.device = device;
    }

    public Data() {

    }

    @Override
    public String toString() {
        return "Data{" +
                "appId='" + appId + '\'' +
                ", devId='" + devId + '\'' +
                ", devIdType='" + devIdType + '\'' +
                ", formates=" + formates +
                ", geo=" + geo +
                ", user=" + user +
                ", device=" + device +
                '}';
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getDevId() {
        return devId;
    }

    public void setDevId(String devId) {
        this.devId = devId;
    }

    public String getDevIdType() {
        return devIdType;
    }

    public void setDevIdType(String devIdType) {
        this.devIdType = devIdType;
    }

    public List<String> getFormates() {
        return formates;
    }

    public void setFormates(List<String> formates) {
        this.formates = formates;
    }

    public Geo getGeo() {
        return geo;
    }

    public void setGeo(Geo geo) {
        this.geo = geo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }
}
