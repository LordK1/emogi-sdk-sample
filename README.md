# Emogi:
To implements SDK in android there are tow major approaches:    
 Third-Party Library: in this method we just develop a module and other application (app integrator) 
    must be use it as a dependency module `compile project(path: ':emosdk')` on the `build.gradle`.  
   
although, It might be something like combination of both of these methods.  

### Base URl :
https://sand-cxp.emogi.com/v1/sessions 


# Usage : :punch:
 Please Follow the steps below for obtaining the EmogiSDK in your application:
 
 1. Download the emogisdk module from the Github repository [here]()  
  
 2. Add the EmogiSDK dependency ( it might be a gradle dependency in artifcatory servers )
 
```
dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
    androidTestCompile('com.android.support.test.espresso:espresso-core:2.2.2', {
        exclude group: 'com.android.support', module: 'support-annotations'
    })
    // EmogiSDK
    compile project(path: ':emosdk')
    // ... Other Dependencies 
}
```

 3. Define the your Application for instance let me call it MainApplication. <br/>
Your `AndroidManifest.xml` might be something like this:

```     <application
            android:name=".MainApplication"
            android:allowBackup="true"
            android:icon="@mipmap/ic_launcher"
            android:label="@string/app_name"
            android:roundIcon="@mipmap/ic_launcher_round"
            android:supportsRtl="true"
            android:theme="@style/AppTheme">
 ```
 
 4. Now we're going to initialize the EmpgiSDK on the `MainApplication.java` :
   ```
    @Override
    public void onCreate() {
        super.onCreate();
           mEmogiSdk =
            new EmogiSdk.Builder(this)
                .promptPermissions(true)
                .logger(true)
                .setDeviceId("Sample") 
                .setUser(new User(22, User.Gender.MALE))
                .build();
    }
   ```
Note: Some initial information must not be null and it's on your side to prepare them.:pray:

 5. In addition you can define a field in your `MainApplication.java` class for any further calls from entire parts of the application with the getter method:
```
    public EmogiSdk getDefaultEmogi() {
        return mEmogiSdk;
    }
```  
  
  6. in your Activity classes let's call it `MainActivity.java`, you can update some previous properties with the new values as you can see below:
```
        // send request and get the data
        ((MainApplication) getApplication()).getDefaultEmogi().getData(new EmogiResponseHandler());
```    
 and also you need to implement `com.k1.emosdk.EmogiResponseHandler` like below
 ```
 /**
      * When You getting back the data from {@link EmogiSdk}.
      * Handling the result use cases in on your side, such as run on ui thread or whatever you wanted !
      */
     private class EmogiResponseHandler implements com.k1.emosdk.EmogiResponseHandler {
         @Override
         public void onSuccess(final EmogiResponse response) {
             Log.d(TAG, "onSuccess() called with: response = [" + response + "]");
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     mTextView.setText(response.toString());
                 }
             });
         }
 
         @Override
         public void onError(final EmogiError error) {
             Log.e(TAG, "onError: >>> ERROR : " + error);
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     mTextView.setText(error.toString());
                 }
             });
         }
 
     }
 ```
 Note: be aware of asynchronous and runOnUiThread in your activity getting back the data.