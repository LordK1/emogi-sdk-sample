package com.k1.emogisdk;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.test.mock.MockApplication;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.k1.emosdk.EmogiError;
import com.k1.emosdk.EmogiResponse;
import com.k1.emosdk.EmogiSdk;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Simple implementation of the {@link EmogiSdk} to overwrite configurations
 * i.e: Location , DeviceID.
 * <p>
 * I've just decided to implement both methods of finding locations via Google Location API ,
 * and Android Location Service. Our sdk must be compatible with both of these methods. I hope :)
 * </p>
 *
 * @author K1
 */
public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private static final int FASTEST_INTERVAL = 5 * 1000;
    private static final int INTERVAL = 10 * 1000;

    private static final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.main_activity_text_view)
    TextView mTextView;

    private GoogleApiClient mGoogleApiClient;
    private Location mCurrentLocation;
    private LocationRequest mLocationRequest;
    private LocationListener mLocationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // it's optional
        ButterKnife.bind(this);
        // Location Listener
        mLocationListener = new LocationListener();
        // Configure google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        // Check location manager service
        final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        final Criteria criteria = new Criteria();
        final List<String> allProviders = locationManager.getAllProviders();
        Log.d(TAG, "onCreate: >>>> allProviders : " + allProviders + " <<<<");
        final String bestProvider = locationManager.getBestProvider(criteria, false);
        Log.d(TAG, "onCreate: >>>> bestProvider : " + bestProvider + " =-=-=-=");
        if (bestProvider != null && !TextUtils.isEmpty(bestProvider)) {
            try {
                final Location lastKnownLocation = locationManager.getLastKnownLocation(bestProvider);
                Log.d(TAG, "onCreate: >>> lastKnownLocation : " + lastKnownLocation + " <<<<");

                locationManager.requestLocationUpdates(bestProvider, 1500, 1, mLocationListener);
            } catch (SecurityException e) {            // check permissions
                e.printStackTrace();
                ActivityCompat.requestPermissions(this,
                        new String[]{
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_FINE_LOCATION,

                        }
                        , 1010);
            }
        }


        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                final AdvertisingIdClient.Info advertisingIdInfo;
                try {
                    advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(getApplicationContext());
                    Log.d(TAG, "onCreate: -------------- advertisingIdInfo : " + advertisingIdInfo + " =-=-========");
                    ((MainApplication) getApplication()).getDefaultEmogi().setDevId(advertisingIdInfo.getId());
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                }
                return null;
            }

        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    /*@OnClick(R.id.main_activity_goto_button)
    public void onGotoSecondActivityClicked() {
        SecondActivity.start(this);
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.main_menu_reload_action) {
            reloadData();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * To reload data manually and update {@link EmogiSdk} data for new situation
     */
    @OnClick(R.id.main_activity_goto_button)
    void reloadData() {
        Log.d(TAG, "reloadData() called > "
                + " instance: " + ((MainApplication) getApplication()).getDefaultEmogi()
        );
        // send request and get the data
        ((MainApplication) getApplication()).getDefaultEmogi().getData(new EmogiResponseHandler());


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // now you can update the last location on the EmogiSdk, which is accessible from two ways
        Log.d(TAG, "onRequestPermissionsResult() called with: requestCode = [" + requestCode
                + "], permissions = [" + permissions + "], grantResults = [" + grantResults + "]");


    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
        Log.d(TAG, "onStart() called");
        // If you want to handle request permissions, just do it as yourself method i.e:
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
            }, 1009);
        }
    }


    @Override
    protected void onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected() called with: bundle = [" + bundle + "]");
        // Begin polling for new location updates.
        startLocationUpdates();

    }

    /**
     * Trigger new location updates at interval
     */
    private void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(INTERVAL)    //10 secs
                .setFastestInterval(FASTEST_INTERVAL); // 2 secs
        // Request location updates
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new GMSLocationListener());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended() called with: i = [" + i + "]");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed() called with: connectionResult = [" + connectionResult + "]");
    }

    /**
     * Both methods Google and Android Location services must update {@link EmogiSdk#lastLocation}
     * field to gain more relevant data !!!
     *
     * @param location
     */
    private void updateEmogiLocation(Location location) {
        ((MainApplication) getApplication()).getDefaultEmogi().onLocationChanged(location);
    }

    /**
     * To collect the data and listen to location changes,
     * it might be recommended that you use {@link android.content.BroadcastReceiver}
     * it handle some unexpected overheads !
     *
     * @author K1
     */
    private class LocationListener implements android.location.LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG, "onLocationChanged() called with: location = [" + location + "]");
            updateEmogiLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d(TAG, "onStatusChanged() called with: provider = [" + provider + "], status = [" + status + "], extras = [" + extras + "]");
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d(TAG, "onProviderEnabled() called with: provider = [" + provider + "]");
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.d(TAG, "onProviderDisabled() called with: provider = [" + provider + "]");
        }
    }

    /**
     * Google Location Api services has different {@link LocationListener}
     */
    private class GMSLocationListener implements com.google.android.gms.location.LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG, "GMSLocationListener onLocationChanged() called with: location = [" + location + "]");
            updateEmogiLocation(location);
        }
    }

    /**
     * When You getting back the data from {@link EmogiSdk}.
     * Handling the result use cases in on your side, such as run on ui thread or whatever you wanted !
     */
    private class EmogiResponseHandler implements com.k1.emosdk.EmogiResponseHandler {
        @Override
        public void onSuccess(final EmogiResponse response) {
            Log.d(TAG, "onSuccess() called with: response = [" + response + "]");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTextView.setText(response.toString());
                }
            });
        }

        @Override
        public void onError(final EmogiError error) {
            Log.e(TAG, "onError: >>> ERROR : " + error);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTextView.setText(error.toString());
                }
            });
        }

    }
}
