package com.k1.emogisdk;

import android.app.Application;

import com.k1.emosdk.EmogiSdk;
import com.k1.emosdk.model.EmogiAdFormat;
import com.k1.emosdk.model.User;

/**
 * Created by K1 on 5/13/17.
 */
public class MainApplication extends Application {

    private EmogiSdk mEmogiSdk;

    @Override
    public void onCreate() {
        super.onCreate();

        mEmogiSdk =
                new EmogiSdk.Builder(this)
                        .promptPermissions(true)
                        .logger(true)
                        .setAppID("1FUUBK") // It's on the integrator side to provide the value
                        .setDeviceId("AA7E87A1-8A50-426B-8361-6F74977E8FD0") // it's by the integrator to provide some initial values
                        .setAdFormats(EmogiAdFormat.ALL)
                        .setmUser(new User(22, User.Gender.MALE))
                        .build();


    }

    /**
     * in entire part of the application you must be used this instance of {@link EmogiSdk}
     * and also you can update some fields and properties further via this one.
     *
     * @return
     */
    public EmogiSdk getDefaultEmogi() {
        return mEmogiSdk;
    }


}
