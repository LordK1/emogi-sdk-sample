package com.k1.emogisdk;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Just for some extra tests,
 * It doesn't matter which activity should have ran EmogiSdk,
 * It must be work as well as possible in all activities or situations.
 *
 * @author K1
 */
public class SecondActivity extends AppCompatActivity {

    public static void start(Context context) {
        Intent starter = new Intent(context, SecondActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }
}
